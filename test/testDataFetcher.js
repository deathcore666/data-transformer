const oracledb = require('oracledb');
const config = require('config');
const logger = require('askarteklogger');
const DataFetcher = require('../src/oracle/data-fetcher');

const QueryBuilder = require('../src/model/queryBuilder');
const task = {
    table_data: { table: 'calls',
        date_from: '2018-01-01',
        date_to: '2018-01-01',
        field: 'balance.credit' },

    msisdn: '0700900900',
};

let dataStream1;

const main = async () => {
    await logger.init();
    let poolConfigs = config.get('oracledb');
    let oracleConnectionPool = await oracledb.createPool(poolConfigs);
    let dataFetcher = new DataFetcher(oracleConnectionPool, logger);
    await dataFetcher.connect();
    let buidler = new QueryBuilder(task);

    buidler.build();
    let query = buidler.getQuery;

    // console.log('query:', query.queryString);
    dataStream1 = await dataFetcher.fetchBalanceCreditDataStream(query.queryString, query.queryParams);
};

main()
    .then(()=> {
        dataStream1.on('data', (message) => {
            console.log('message:', message);
        });

        dataStream1.on('end', () => {
            console.log('END');
            //Этот тоже
            // let t = ((Date.now() - startTime)/1000);
            // console.log('queryStream():' + t);

        });

        dataStream1.on('error', (err) => {
            console.log('ERROR while streaming:', err);
        })
    })
.catch(err=>console.log('ERROR:', err));
