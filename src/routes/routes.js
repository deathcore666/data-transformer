const express = require('express');
const route = express.Router();
const healthcheck = require('../controller/health');

route.use( (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'POST');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

route.post('/api/health', healthcheck.health);

module.exports = route;
