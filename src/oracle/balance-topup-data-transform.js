const { Transform } = require('stream');

const BEELINE = require('../constants/beeline');

class BalanceTopUpDataTransform {
    constructor(balanceTopUpColumnsMap, taskId) {
        this.transformBalanceTopUpData = new Transform({
            readableObjectMode: true,
            writableObjectMode: true,

            transform(record, encoding, callback) {
                const EVENT_DT = balanceTopUpColumnsMap[BEELINE.EVENT_DT];
                const RECHARGE_VAL = balanceTopUpColumnsMap[BEELINE.RECHARGE_VAL];
                const SUBS_KEY = balanceTopUpColumnsMap[BEELINE.SUBS_KEY];

                let msg = { data    : {
                        taskId          : taskId,
                        topupDate       : record[EVENT_DT],
                        topupAmount     : record[RECHARGE_VAL],
                        accountId       : record[SUBS_KEY]
                    },

                    key     : Date.now() + record[SUBS_KEY],
                };

                this.push(msg);
                callback();
            }
        });
    }

    get transformer() {
        return this.transformBalanceTopUpData;
    }
}

module.exports = BalanceTopUpDataTransform;