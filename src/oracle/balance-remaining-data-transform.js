const { Transform } = require('stream');

const BEELINE = require('../constants/beeline');

class BalanceRemainingDataTransform {
    constructor(balanceRemainingColumnsMap, taskId) {
        this.transformBalanceRemainingData = new Transform({
            readableObjectMode: true,
            writableObjectMode: true,

            transform(record, encoding, callback) {
                const EVENT_DT = balanceRemainingColumnsMap[BEELINE.EVENT_DT];
                const BALANCE_NVAL = balanceRemainingColumnsMap[BEELINE.BALANCE_NVAL];
                const SUBS_KEY = balanceRemainingColumnsMap[BEELINE.SUBS_KEY];

                let msg = { data: {
                        taskId          : taskId,
                        topupDate       : record[EVENT_DT],
                        topupAmount     : record[BALANCE_NVAL],
                        accountId       : record[SUBS_KEY]
                    },

                    key     : Date.now() + record[SUBS_KEY],
                };
                this.push(msg);
                callback();
            }
        });
    }

    get transformer() {
        return this.transformBalanceRemainingData;
    }
}

module.exports = BalanceRemainingDataTransform;