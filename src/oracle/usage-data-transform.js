const { Transform } = require('stream');

const BEELINE = require('../constants/beeline');

class UsageDataTransformStream {
    constructor(usageColumnsMap, taskId) {
        this.transformUsageData = new Transform({
            readableObjectMode: true,
            writableObjectMode: true,

            transform(record, encoding, callback) {
                let msg = {};
                const CALL_TYPE_KEY = usageColumnsMap[BEELINE.CALL_TYPE_KEY];
                const COUNTRY_KEY = usageColumnsMap[BEELINE.COUNTRY_KEY];
                const B_COUNTRY_KEY = usageColumnsMap[BEELINE.B_COUNTRY_KEY];
                const START_DT = usageColumnsMap[BEELINE.START_DT];
                const CHARGE_NVAL = usageColumnsMap[BEELINE.CHARGE_NVAL];
                const PHONE_NUM = usageColumnsMap[BEELINE.PHONE_NUM];
                const B_PHONE_NUM = usageColumnsMap[BEELINE.B_PHONE_NUM];
                const CELL_NVAL = usageColumnsMap[BEELINE.CELL_NVAL];
                const IMEI_CVAL = usageColumnsMap[BEELINE.IMEI_CVAL];
                const SUBS_KEY = usageColumnsMap[BEELINE.SUBS_KEY];
                const DURATION_NVAL = usageColumnsMap[BEELINE.DURATION_NVAL];
                const DATA_VOL_NVAL = usageColumnsMap[BEELINE.DATA_VOL_NVAL];
                const PRICE_PLAN_KEY = usageColumnsMap[BEELINE.PRICE_PLAN_KEY];

                //SMS_TOPIC
                if (record[CALL_TYPE_KEY] === 1191) {
                    msg = {
                        data: {
                            taskId          : taskId,
                            textDate        : record[START_DT],
                            textTypeKey     : record[B_COUNTRY_KEY] === 895 ? 'local' : 'international',
                            textPrice       : record[CHARGE_NVAL],
                            phoneNumFrom    : record[PHONE_NUM],
                            phoneNumTo      : record[B_PHONE_NUM],
                            locationKey     : record[CELL_NVAL],
                            senderImei      : record[IMEI_CVAL],
                        },
                        key     : Date.now() + record[SUBS_KEY],

                    };
                    this.push(msg);
                }

                //CALLS_TOPIC
                if (record[CALL_TYPE_KEY] >= 1160 && record[CALL_TYPE_KEY] <= 1189) {
                    msg = {
                        data: {
                            taskId          : taskId,
                            callDate        : record[START_DT],
                            callTypeKey     : record[B_COUNTRY_KEY] !== 895 ? 'international' :
                                record[CALL_TYPE_KEY] === 1171 ? 'outsideNetworkCAll' : 'networkCAll',
                            callDuration    : record[DURATION_NVAL],
                            callPrice       : record[CHARGE_NVAL],
                            phoneNumFrom    : record[PHONE_NUM],
                            phoneNumTo      : record[B_PHONE_NUM],
                            locationKey     : record[CELL_NVAL],
                            callerImei      : record[IMEI_CVAL],
                        },
                        key     : Date.now() + record[SUBS_KEY],

                    };
                    this.push(msg);
                }

                //Internet Logs
                if (record[CALL_TYPE_KEY] === 1205 && record[DATA_VOL_NVAL] !== 0) {
                    msg = {
                        data: {
                            taskId          : taskId,
                            usageDate       : record[START_DT],
                            phoneNum        : record[PHONE_NUM],
                            dataVolume      : record[DATA_VOL_NVAL],
                            dataPrice       : record[CHARGE_NVAL],
                            hostIp          : 'N/A'
                        },
                        key     : Date.now() + record[SUBS_KEY],

                    };
                    this.push(msg);
                }

                // Tarification
                msg = { data: {
                        taskId              : taskId,
                        usageDate           : record[START_DT],
                        phoneNum            : record[PHONE_NUM],
                        pricePlanKey        : record[PRICE_PLAN_KEY],
                        imei                : record[IMEI_CVAL],
                    },
                    key     : Date.now() + record[SUBS_KEY],

                };
                this.push(msg);
                callback();
            }
        });
    }

    get transformer() {
        return this.transformUsageData;
    }
}

module.exports = UsageDataTransformStream;