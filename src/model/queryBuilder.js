const knex = require('knex')({client: 'oracle'});

class QueryBuilder {
    constructor(task) {
        this.task = task;
        this.query = {
            queryString: '',
            queryParams: []
        };

        this.domainHandler = {
            //Usage table
            'sms': () => {
                let tableData = this.task['table_data'];

                this.query.queryString += knex('USAGE')
                    .where({
                        // START_DT: tableData['date_from'],
                        // END_DT: tableData['date_to'],
                        CALL_TYPE_KEY: 1191
                    })
                    .select();
            },

            'call': () => {
                let tableData = this.task['table_data'];

                this.query.queryString += knex('USAGE')
                    .whereBetween('CALL_TYPE_KEY', [1160, 1189])
                    .andWhere({
                        // START_DT: tableData['date_from'],
                        // END_DT: tableData['date_to'],
                    })
                    .select();
            },

            'internet': () => {
                let tableData = this.task['table_data'];

                this.query.queryString += knex('USAGE')
                    .where('DATA_VOL_NVAL', '!=', 0)
                    .andWhere({
                        // START_DT: tableData['date_from'],
                        // END_DT: tableData['date_to'],
                        CALL_TYPE_KEY: 1205
                    })
                    .select();
            },

            //Balance table
            'topup': () => {
                this.query.queryString += knex('PAYMENT')
                    .where({
                        // ACCOUNT_KEY: this.task['msisdn']
                    })
                    .select();
            },

            'credit': () => {
                this.query.queryString += knex('TRUSTPAYMENT')
                    .where({
                        // SUBS_KEY: this.task['msisdn']
                    })
                    .select();
            },

            'remaining': () => {
                this.query.queryString += knex('BALANCE')
                    .where({
                        // ACCOUNT_KEY: this.task['msisdn']
                    })
                    .select();
            }
        };
    }

    build() {
        let tableData = this.task['table_data'];

        let domain = tableData['field'].split('.');

        if (domain[0] === 'balance') {
            this.domainHandler[domain[1]]();
        }

        if (domain[0] === 'local' || domain[0] === 'roaming') {
            this.domainHandler[domain[1]]();
        }
    }

    get getQuery() {
        return this.query;
    }
}

module.exports = QueryBuilder;