const EventEmitter = require('events');

const BEELINE = require('../constants/beeline');
const DataFetcher = require('../oracle/data-fetcher');
const QueryBuilder = require('../model/queryBuilder');

// Потом удалю
// let startTime;

/**
 * Information about user's balance is stored in three different views: 'payment', 'trustpayment' and 'balance'
 * Tasks that require info residing in those views has to have following structure
 * * balance.<remaining/credit/topup>
 */

class Processor extends EventEmitter {
    constructor(logger, task, kafkaProducer) {
        super();
        this.logger = logger;
        this.task = task;

        this.dataFetcher = new DataFetcher(logger, this.task);
        this.kafkaProducer = kafkaProducer;

        this.dataStream = null;
        this.queryBuilder = new QueryBuilder(this.task);
        this.query = {};

        this.topic = '';


        this.tableDataHandler = {

            'local': {
                'call': async () => {
                    this.topic = BEELINE.CALLS_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'sms': async () => {
                    this.topic = BEELINE.SMS_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'internet': async () => {
                    this.topic = BEELINE.INTERNET_LOGS_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'tarificaion': async () => {
                    this.topic = BEELINE.TARIFICATION_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                }
            },

            'roaming': {
                'call': async () => {
                    this.topic = BEELINE.CALLS_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'sms': async () => {
                    this.topic = BEELINE.ROAMING_SMS_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'internet': async () => {
                    this.topic = BEELINE.ROAMING_INTERNET_LOGS_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                },

                'tarificaion': async () => {
                    this.topic = BEELINE.ROAMING_TARIFICATION_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchUsageDataStream(this.query.queryString, this.query.queryParams);
                }
            },

            'balance': {
                'topup': async () => {
                    this.topic = BEELINE.BALANCE_TOPUP_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchBalanceTopUpDataStream(this.query.queryString, this.query.queryParams);
                },

                'credit': async () => {
                    this.topic = BEELINE.BALANCE_CREDIT_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchBalanceCreditDataStream(this.query.queryString, this.query.queryParams);
                },

                'remaining': async () => {
                    this.topic = BEELINE.BALANCE_REMAINING_TOPIC;
                    this.dataStream = await this.dataFetcher.fetchBalanceRemainingDataStream(this.query.queryString, this.query.queryParams);
                },

            },
        }
    }

    start() {
        const main = async () => {
            await this.dataFetcher.connect();

            this.queryBuilder.build();
            this.query = this.queryBuilder.getQuery;

            await this.fetchStream();
            this.sendData();
        };

        return main()
            .catch(err => {
                this.logger.logError('Error starting a child process. ' + err, this.task.taskId);
                console.error('Error starting a child process. ' + err, this.task.taskId);
            });
    }

    async fetchStream() {
        const main = async () => {
            let domain = this.task['table_data']['field'].split('.');
            await this.tableDataHandler[domain[0]][domain[1]]();
        };

        return main()
            .catch(err => {
                this.logger.logError('Error fetching data stream. ' + err, this.task.taskId);
                console.error('Error fetching data stream. ' + err);
            });
    }

    sendData() {
        //Мне нужен этот код для настроек Oracle
        //Потом удалю
        // startTime = Date.now();
        // console.log('this.dataStream:', this.dataStream);

        if (!this.dataStream) {
            this.dataFetcher.releaseConnection();
            this.emit('endChild');
            return;
        }

        this.dataStream.on('data', (message) => {
            try {
                this.kafkaProducer.produce(this.topic, null, new Buffer(JSON.stringify(message.data)), message.key, Date.now())
            } catch (err) {
                console.error('Error producing a message to kafka: ', err);
                this.logger.logError('Error producing a message to kafka: ' + err, this.task.taskId);
            }
        });

        this.dataStream.on('end', async () => {
            try {
                this.kafkaProducer.produce(this.topic, null, new Buffer('EOF'), null, Date.now())
            } catch (err) {
                console.error('Error producing an EOF message to kafka: ', err);
                this.logger.logError('Error producing an EOF message to kafka: ' + err, this.task.taskId);
            }

            this.dataStream.end();
            this.dataFetcher.releaseConnection();
            this.emit('endChild');

            //Этот тоже
            // let t = ((Date.now() - startTime)/1000);
            // console.log('queryStream():' + t);
        });

        this.dataStream.on('error', async (err) => {
            this.dataFetcher.releaseConnection();
            this.dataStream.end();
            this.emit('errorChild', err);
        })
    }

    cancelTask() {
        this.dataStream.end();
        this.dataFetcher.releaseConnection();
    }

    shutdown() {
        this.dataFetcher.releaseConnection();
    }
}

module.exports = Processor;