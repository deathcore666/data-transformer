const EventEmitter = require('events');

const Process = require('./processor');

class PreProcessor extends EventEmitter {
    constructor(logger, task, kafkaProducer) {
        super();
        this.logger = logger;
        this.task = task;
        this.kafkaProducer = kafkaProducer;
        this.childProcesses = [];
        this.childProcessOverCounter = 0;
    }

    async start() {
        // console.log('table_data:', this.task['table_data'], this.task.task_id);
        // let tableData = JSON.parse(this.task['table_data']);
        let tableData = (this.task['table_data']);

        for (let i in tableData) {
            let task = {};
            task['table_data'] = tableData[i];
            task['msisdn'] = this.task['msisdn'];
            task['taskId'] = this.task['task_id'];

            let childProcess = new Process(this.logger, task, this.kafkaProducer);
            this.childProcesses.push(childProcess);

            childProcess.on('errorChild', (err) => {
                this.childProcessOverCounter++;

            });

            childProcess.on('endChild', () => {
                this.childProcessOverCounter++;

                if (this.childProcessOverCounter === (this.childProcesses.length)) {
                    this.emit('endParent');
                }
            });
            await childProcess.start();
        }
    }

    async cancelTask() {
        for (let i in this.childProcesses) {
            this.childProcesses[i].cancelTask();
        }

        this.emit('cancelParent');
    }

    shutdown() {
        for (let i in this.childProcesses) {
            this.childProcesses[i].shutdown();
        }
    }
}

module.exports = PreProcessor;