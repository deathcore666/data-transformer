const config = require('config');
const express = require('express');
const bodyParser = require('body-parser');


const logger = require('askarteklogger');
const taskManager = require('task_manager');
const {Producer} = require('rib-kafka');

const Process = require('./src/controller/processor');
const configValidator = require('./config/config');

require('dotenv').config({path: '.env'});
const routes = require('./src/routes/routes');
const app = express();

let componentName = '';
let taskId = '';
let JOBS_LIMIT = null;
let componentIdFilePath = '';
let producerConfigs;
let componentID;
let jobsQueue = [];

let producer;

async function init() {
    const main = async () => {
        await configValidator.validate();
        await logger.init();

        componentName = config.get('componentName');
        taskId = config.get('componentInitTaskId');
        JOBS_LIMIT = config.get('JOBS_LIMIT');
        componentIdFilePath = config.get('componentIdFilePath');
        producerConfigs = config.get('kafka');
        componentID = process.env.containerId;

        await taskManager.init(onError, componentID);

        producer = new Producer({config: producerConfigs});
        producer.on('error', onError);
        await producer.connect();
        await taskManager.serviceStart(componentID);

        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended: false}));
        app.use('/', routes);
        app.set('port', process.env.PORT || 8080);
        const server = app.listen(app.get('port'), () => {
            console.log(`Express running → PORT ${server.address().port}`);
            logger.logInfo(`Express running → PORT ${server.address().port}`, 'M7-3 Service init')
        });
    };

    main()
        .then(() => {
            logger.logInfo('An instance of M1-1 has been started successfully.', taskId);
            console.log('An instance of M1-1 ' + componentID + ' has been started successfully.');
            setInterval(checkTasksQueue, 1500);
        })
        .catch(err => {
            console.error('Error on service init: ', err)
        })

}

function checkTasksQueue() {
    let _tasksQueue = taskManager.getTasks();

    _tasksQueue.forEach(async task => {
        if (task['status'] === 'inQueue' && jobsQueue.length < JOBS_LIMIT) {
            try {
                let process = new Process(logger, task, producer);
                jobsQueue.push(process);

                await taskManager.inProgress(task['task_id'], componentID);

                process.on('endParent', async () => {
                    logger.logInfo('Job done', task['task_id']);
                    await taskManager.success(task['task_id']);
                    removeTask(process.task);
                });

                process.on('cancelParent', async () => {
                    logger.logInfo('Job cancelled', task['task_id']);
                    await taskManager.cancelled(task['task_id']);
                });

                process.on('errorParent', async (err) => {
                    logger.logError('Error in kafka stream. Error: ' + err, this.task.taskId);
                    await taskManager.fail(this.task.taskId);
                });

                await process.start();

            } catch (err) {
                console.log('ERROR at FOREACH: ', err);
            }
        }

        if (task['task_status'] === 'cancel') {
            cancelTask(task)
                .catch(err => {
                    logger.logError('Error cancelling task' + err, task.taskId);
                })
        }
    });
}

function removeTask(task) {
    for (let i in jobsQueue) {
        if (task['task_id'] === jobsQueue[i].task['task_id']) {

            jobsQueue.splice(i, 1);
        }
    }
}

async function cancelTask(task) {
    //Searching for the job to be cancelled in running jobs
    for (let i in jobsQueue) {
        if (jobsQueue[i].task['task_id'] === task.task_id) {
            await jobsQueue[i].cancelTask();
            jobsQueue.splice(i, 1);
        }
    }
}

function onError(err) {
    logger.logError('Error: ' + err, taskId);
    console.error(err);
}

function shutdown() {
    producer.disconnect();
    taskManager.shutdown();
    //logger.shutdown();

    for (let i in jobsQueue) {
        jobsQueue[i].shutdown();
    }
}

process.once('SIGTERM', shutdown);
process.once('SIGINT', shutdown);
process.once('SIGHUP', shutdown);

init();

// setInterval(function(){}, 1000);