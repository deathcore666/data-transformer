#!/bin/bash

# Create directory for example: scoring
# Copy this file into created directory
# Run script sh run_services.sh

export HOME=$(pwd)


git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m1-1
cd $HOME
mv data-source/ M1-1/
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m1-3
cd $HOME
mv data-source/ M1-3/
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m1-4
cd $HOME
mv data-source/ M1-4
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m2
cd $HOME
mv data-source/ M2
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m2-1
cd $HOME
mv data-source/ M2-1
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m2-2
cd $HOME
mv data-source/ M2-2
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m2-3
cd $HOME
mv data-source/ M2-3
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m4
cd $HOME
mv data-source/ M4
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m4-1
cd $HOME
mv data-source/ M4-1
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m4-2
cd $HOME
mv data-source/ M4-2
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m4-3
cd $HOME
mv data-source/ M4-3
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m4-4
cd $HOME
mv data-source/ M4-4
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m4-5
cd $HOME
mv data-source/ M4-5
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m4-6
cd $HOME
mv data-source/ M4-6
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-m5
cd $HOME
mv data-source/ M5
sleep 3

git clone git@bitbucket.org:askartec/data-source.git
cd $HOME/data-source
git checkout development-mz
cd $HOME
mv data-source/ MZ

sleep 3

cp -r $HOME/M1-1/m1-1/docker-compose.yaml  $HOME/docker-compose.yaml

docker-compoce up -d --build

exec "$@"