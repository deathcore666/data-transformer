#!/usr/bin/env bash

echo >> .env
echo -n CONTAINER_ID= > .env
echo `cat /proc/self/cgroup | grep 1:name | cut -c 24-35` >> .env

npm start